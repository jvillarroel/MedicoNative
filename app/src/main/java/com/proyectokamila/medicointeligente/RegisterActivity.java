package com.proyectokamila.medicointeligente;


import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {
    private TextInputLayout textInputName;
    private TextInputLayout textInputEmail;
    private TextInputLayout textInputPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        textInputName = findViewById(R.id.text_input_name);
        textInputEmail = findViewById(R.id.text_input_email);
        textInputPassword = findViewById(R.id.text_input_password);
    }

    public void message(String x){
        Toast.makeText(this, x, Toast.LENGTH_SHORT).show();
    }

    public void CreateAcount(View v) { //funcion principal
        String nameInput = textInputName.getEditText().getText().toString();
        String emailInput = textInputEmail.getEditText().getText().toString();
        String passwordInput = textInputPassword.getEditText().getText().toString();
        LoginActivity login = new LoginActivity();

        if(!login.validateEmpty(emailInput) | !login.validateEmpty(passwordInput) | !login.validateEmpty(nameInput)){
            message("No pueden haber campos vacios");
        }else{
            if(!login.isEmail(emailInput)){
                message("Correo invalido");
            }
            else{
                message("Registrado!");
                Intent miIntent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(miIntent);
            }
        }
    }
}