package com.proyectokamila.medicointeligente;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity {
    private TextInputLayout textInputEmail;
    private TextInputLayout textInputPassword;
    private Socket mSocket;

    {
        try {
            mSocket = IO.socket("https://v1.corek.io:8096");
        } catch (URISyntaxException e) {}
    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            System.out.println("acaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +args[0]);
        }
    };

    public void socketon(String event){

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String x = "connection";
        mSocket.on("connection",new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                JSONObject object = new JSONObject();

                JSONObject obj = new JSONObject();
                try {
                    obj.put("project","http://medicointeligente.com");
                    mSocket.emit("conf",obj);
                    mSocket.on("conf",new Emitter.Listener() {

                        @Override
                        public void call(Object... args) {
                            System.out.println("KKKKKKKKKKKKKKKKKKKKKKKKKKKKK" +args[0]);
                            JSONObject object = new JSONObject();

                            JSONObject obj = new JSONObject();
                            try {
                                obj.put("project","medicointeligente.com");
//                                mSocket.emit("get_users",obj);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        mSocket.connect();

        setContentView(R.layout.activity_login);

        textInputEmail = findViewById(R.id.text_input_email);
        textInputPassword = findViewById(R.id.text_input_password);
    }

    public boolean isEmail(String email){
        Pattern pat = null;
        Matcher mat = null;

        pat = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        mat = pat.matcher(email);

        if(mat.find()){
            return true;
        }else{
            return false;
        }
    }

    protected void CreateAcount(View view) {
        Intent miIntent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(miIntent);
    }

    public boolean validateEmpty(String x){
        if (x.isEmpty()){
            return false;
        }else{
            return true;
        }
    }

    public void message(String x){
        Toast.makeText(this, x, Toast.LENGTH_SHORT).show();
    }

    public void SingIn(View v) throws JSONException { //funcion principal
        JSONObject obj = new JSONObject();
        JSONObject condition1 = new JSONObject();
        condition1.put("user_login","j.vivas.ubv@gmail.com");
        obj.put("condition",condition1);
        System.out.println("JAAAAAAAAAAAAAAAAAAAAAA" +obj);
        mSocket.emit("get_users",obj);
        mSocket.on("get_users",new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                System.out.println("JAAAAAAAAAAAAAAAAAAAAAA" +args[0]);
                JSONObject data = new JSONObject();
                try {
                    data.put("condition",args[0]);
                    System.out.println("qqqqqqqqqq"+data);
                    System.out.println("Email es "+ data.get("condition"));
                    JSONObject user = new JSONObject();
                    System.out.println("datos  son "+ data.getJSONArray("condition"));
                    data.toJSONArray(data.getJSONArray("condition"));
                    System.out.println("json array "+data.toJSONArray(data.getJSONArray("condition")));

                } catch (JSONException e) {
                    System.out.println("error");
                    e.printStackTrace();
                }
            }
        });

        String emailInput = textInputEmail.getEditText().getText().toString();
        String passwordInput = textInputPassword.getEditText().getText().toString();

        if(!validateEmpty(emailInput) | !validateEmpty(passwordInput)){
            message("No pueden haber campos vacios");
            return;
        }

        if(passwordInput.equals("12345") && (isEmail(emailInput) == true)){
            Intent miIntent = new Intent(LoginActivity.this, DashboardActivity.class);
            startActivity(miIntent);

            String input = "Email: " + emailInput;
            input += "\n";

            input += "Password: " + passwordInput;
            Toast.makeText(this, input, Toast.LENGTH_SHORT).show();
        }

        else if(!isEmail(emailInput)){
            message("Introduzca un correo valido");

        }else{
            message("Datos invalidos");
        }
    }
}